<h1>Admin panel</h1>
<a href="{{ urlFor('logout') }}">Log out</a>

{% if flash.success %}
<p>{{ flash.success }}</p>
{% elseif flash.error %}
<p>Error: {{ flash.error }}</p>
{% endif %}

<h2>All posts</h2>
{% if posts is empty %}
<p>No posts yet.</p>
{% else %}
    {% for post in posts %}
    <div class="post">
      <h3><a href="{{ urlFor('post.show', {'postId': post.id}) }}">{{ post.title }}</a></h3>
      <p>{{ post.text[:100] }}</p>
      <div class="author">
          By {{ post.username }} <a href="{{ urlFor('post.edit', {'postId': post.id})}}">Edit</a> |  <a href="{{ urlFor('post.delete', {'postId': post.id})}}">Delete</a>
      </div>
    </div>
    {% endfor %}
{% endif %}
<br/><br/>
<h3><a href="{{ urlFor('post.form')}}">New post</a></h3>


