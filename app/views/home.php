{% if user is empty %}
<a href="{{ urlFor('login.form') }}">Log in</a>
{% else %}
Welcome, {{ user.username }}. <a href="{{ urlFor('logout') }}">Log out</a>
{% endif %}

{% if flash.success %}
<p>{{ flash.success }}</p>
{% elseif flash.error %}
<p>Error: {{ flash.error }}</p>
{% endif %}

<h1>All posts</h1>

{% if posts is empty %}
<p>No posts yet.</p>
{% else %}
    {% for post in posts %}
    <div class="post">
      <h2><a href="{{ urlFor('post.show', {'postId': post.id}) }}">{{ post.title }}</a></h2>
      <p>{{ post.text[:100] }}</p>
      <div class="author">
        By {{ post.username }} {% if user.id == post.user_id %} | <a href="{{ urlFor('post.edit', {'postId': post.id})}}">Edit</a> |  <a href="{{ urlFor('post.delete', {'postId': post.id})}}">Delete</a>{% endif %}
      </div>
    </div>
    {% endfor %}
{% endif %}

{% if user.id %}
<br/><br/>
<h3><a href="{{ urlFor('post.form')}}">New post</a></h3>
{% endif %}