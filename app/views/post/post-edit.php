<h1>Edit post</h1>

<form method="POST">
  <p><input name="title" type="text" value="{{ post.title }}"></input></p>
  <p><textarea name="text" rows="5">{{ post.text }}</textarea></p>
  <input type="submit" value="Submit"></input>
</form>

{% if flash.error %}
<div>Error: {{ flash.error }}</div>
{% endif %}

<p><a href="{{ urlFor('home') }}">Back</a></p>  