<h1>{{ post.title }}</h1>

<p>{{ post.text }}</p>

<div class="author">
    By {{ post.username }} on {{ post.created_at }}
</div>

<p><a href="{{ urlFor('home') }}">Back</a></p>