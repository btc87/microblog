<?php

session_start();

require '../vendor/autoload.php';
require '../classes/Db.php';
require '../classes/User.php';
require '../classes/Post.php';

$app = new \Slim\Slim([
    'view' => new \Slim\Views\Twig()
        ]);

$app->container->singleton('db', function () {
    return new Db('mysql', 'localhost', 'xbtc_bl', 'xbtc_blu', 'test#pass999');
});
$app->container->set('user', function () use ($app) {
    return new User($app->db);
});

$view = $app->view();
$view->setTemplatesDirectory('../app/views');
$view->parserExtensions = [
    new \Slim\Views\TwigExtension()
];

$app->hook('slim.before.dispatch', function() use ($app) {
    if (isset($_SESSION['lastactivity'])) {
        $app->user->checkTimeout($_SESSION['lastactivity']);
    }
    $user = null;
    if (isset($_SESSION['user'])) {
        $user = $_SESSION['user'];
        $_SESSION['lastactivity'] = time();
    }
    $app->view()->setData('user', $user);
});

require '../routes.php';

$app->run();
