<?php

$app->get('/post/:postId', function ($postId) use ($app) {

    $post = new Post($app->db, $postId);
    
    if (!$post->getPost()) {
        $app->notFound();
    }

    $app->render('post/post.php', [
        'post' => $post->getPost()
    ]);
})->name('post.show');

$app->get('/post-new', function () use ($app) {

    $user = $app->view()->getData('user');

    if ($user) {
        $app->render('post/post-form.php');
    } else {
        $app->redirect($app->urlFor('login.form'));
    }
})->name('post.form');

$app->post('/post-new', function() use ($app) {

    $req = $app->request;

    $title = $req->params('title');
    $text = $req->params('text');

    $user = $app->view()->getData('user');

    $post = new Post($app->db);
    $post->create($title, $text, $user['id']);

    if ($post) {
        $app->redirect($app->urlFor('home'));
    } else {
        $app->flash('error', 'Please try again');
        $app->redirect($app->urlFor('post.form'));
    }
})->name('post.post');

$app->get('/post/:postId/edit', function ($postId) use ($app) {

    $user = $app->view()->getData('user');
    
    $post = new Post($app->db, $postId);
    
    $postArr = $post->getPost();
    
    $req = $app->request;

    $title = $req->params('title');
    $text = $req->params('text');
    
    if (!$postArr || ($user['id']!=$postArr['user_id'] && $user['isAdmin'] != 1)) {
        $app->notFound();
    } else {
        $app->render('post/post-edit.php', [
            'post' => $postArr
        ]);
    }
})->name('post.edit.form');

$app->post('/post/:postId/edit', function ($postId) use ($app) {
    
    $post = new Post($app->db, $postId);
    $postArr = $post->getPost();
    
    $req = $app->request;
    $title = $req->params('title');
    $text = $req->params('text');
    
    if (!$postArr) {
        $app->notFound();
    } else {
        $post->update($postId, $title, $text);
        $app->redirect($app->urlFor('home'));
    }
})->name('post.edit');

$app->get('/post/:postId/delete', function ($postId) use ($app) {

    $user = $app->view()->getData('user');
    
    $post = new Post($app->db, $postId);
    
    $postArr = $post->getPost();
    
    if (!$postArr || ($user['id']!=$postArr['user_id'] && $user['isAdmin'] != 1)) {
        $app->notFound();
    } else {
        $post->delete($postId);
        $app->redirect($app->urlFor('home'));
    }

})->name('post.delete');

