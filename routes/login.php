<?php

$app->get('/login', function() use ($app) {
    if (isset($_SESSION['user'])) {
        $app->redirect($app->urlFor('home'));
    } else {
        $app->render('login.php');
    }
})->name('login.form');


$app->post('/login', function() use ($app) {

    $req = $app->request;

    $username = $req->params('user');
    $password = $req->params('pass');

    $user_id = $app->user->login($username, $password);

    if ($user_id) {
        $_SESSION['lastactivity'] = time();
        $app->redirect($app->urlFor('home'));
    } else {
        $app->flash('error', 'Wrong user or pass');
        $app->redirect($app->urlFor('login.form'));
    }
})->name('login');
