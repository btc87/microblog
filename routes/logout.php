<?php

$app->get("/logout", function() use ($app) {

    $app->view()->setData('user', null);
    $app->user->logout();
    $app->flash('success', 'You have been logged out.');
    $app->redirect($app->urlFor('home'));
    
})->name('logout');