<?php

$app->get('/admin', function () use ($app) {

    $user = $app->view()->getData('user');

    $posts = $app->db->raw("
        SELECT *, posts.id as id
        FROM posts
        LEFT JOIN users
        ON posts.user_id = users.id
    ")->fetch_all(false);

    if ($user['isAdmin'] == 1) {
        $app->render('admin.php', [
            'posts' => $posts
        ]);
    } else {
        $app->redirect($app->urlFor('login.form'));
    }
})->name('admin');
