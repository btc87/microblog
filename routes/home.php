<?php

$app->get('/', function () use ($app) {

    $user = $app->view()->getData('user');

    $posts = $app->db->raw("
        SELECT *, posts.id as id
        FROM posts
        LEFT JOIN users
        ON posts.user_id = users.id
    ")->fetch_all(false);

    if ($user['isAdmin'] == 1) {
        $app->redirect($app->urlFor('admin'));
    } else {
        $app->render('home.php', [
            'posts' => $posts,
            'user' => $user
        ]);
    }
})->name('home');
