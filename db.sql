-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 21, 2017 at 10:54 AM
-- Server version: 5.6.35-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xbtc_bl`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `text` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `text`, `user_id`, `created_at`) VALUES
(1, 'First post', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porttitor quis felis non bibendum. Vivamus a vehicula elit. Phasellus diam ligula, aliquam vel lobortis sit amet, auctor vitae nisl. Nunc euismod non nisi vitae posuere. Nam quis erat ut dolor eleifend rhoncus in sed erat. Suspendisse cursus ipsum tincidunt sem iaculis malesuada. Donec erat augue, tempor sit amet lectus vitae, blandit tincidunt lectus. Curabitur nisl tellus, ornare sit amet pulvinar a, suscipit in nunc. Morbi volutpat ac nisi faucibus sagittis.\n\nCurabitur mattis venenatis magna, ac dapibus massa pharetra porta. Aliquam augue sem, varius ut interdum at, maximus a enim. Nam arcu orci, rutrum sed dolor sed, porta auctor mauris. Nunc condimentum magna in ligula tristique rutrum. Sed lobortis nec lorem at tincidunt. Sed suscipit sagittis gravida. Vestibulum lacinia pulvinar ornare. Integer pellentesque vestibulum risus et tempor.\n\nPhasellus velit ligula, faucibus vel facilisis in, viverra at nisi. Praesent ut gravida mi. Donec sed auctor ligula, id lobortis enim. Phasellus libero nisi, sollicitudin vitae nunc in, posuere porta mauris. Vivamus felis ex, vehicula pharetra blandit eu, laoreet eu felis. Ut elit dolor, placerat eu tristique eu, iaculis ac justo. Aenean egestas risus sem, et tempus erat gravida sed. Curabitur aliquet, enim vitae mollis aliquam, ipsum tellus sollicitudin urna, eu tincidunt elit erat in enim. Duis fringilla id sapien at egestas. Cras id nisi quis ante aliquet imperdiet ac eget turpis. Maecenas pretium lacinia nulla, non feugiat lacus ultricies eu. Aliquam mi mauris, ullamcorper ultrices accumsan a, sagittis ut ipsum. Integer sed lacus vitae magna congue pulvinar. Sed fringilla lectus quis dui mattis, non vestibulum ante varius. Sed semper cursus nisi, ac ornare purus viverra id. Fusce tincidunt tincidunt nulla vel interdum.', 1, '2017-06-19 15:03:05'),
(2, 'Test post', 'Maecenas molestie in mi a malesuada. Suspendisse rutrum ex eu diam iaculis, ac porttitor augue fermentum. Mauris purus ligula, semper fringilla enim id, cursus ultrices tellus. Praesent libero sem, feugiat nec ullamcorper ac, tincidunt cursus dolor. Suspendisse interdum urna dui, ut luctus erat tincidunt ut. Donec fermentum lacus ac ex luctus mollis. Vivamus vel velit id tortor pharetra pulvinar id vitae diam.\n\nFusce scelerisque consequat congue. Donec metus nibh, sollicitudin in ante quis, bibendum malesuada augue. Cras et massa malesuada, sollicitudin eros nec, cursus nulla. Donec dolor dolor, pellentesque sed augue at, ultrices ultricies neque. Ut at fringilla tortor. Sed vitae purus sollicitudin, lacinia nulla sit amet, ornare metus. Aliquam a elementum eros. Suspendisse rutrum arcu sit amet est fermentum eleifend. Fusce eu tempor lacus. Nam aliquam eros non mauris semper, at porttitor metus interdum.', 1, '2017-06-19 16:11:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `isAdmin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `created`, `isAdmin`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'user@domain.com', '2013-11-18 18:21:03', 1),
(3, 'testuser', 'e10adc3949ba59abbe56e057f20f883e', 'test@test.com', '2017-06-20 14:09:03', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

