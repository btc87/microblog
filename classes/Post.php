<?php

/**
 * Description of Post
 *
 * @author Vladimir
 */
class Post {

    protected $_db;
    private $asArray;

    /**
     * Constructor
     * @param Db $_db     Db object
     */
    public function __construct(Db $db, $postId = null) {
        $this->_db = $db;
        if ($postId != null) {
            $post = $this->_db->query("
                    SELECT *
                    FROM posts
                    LEFT JOIN users
                    ON posts.user_id = users.id
                    WHERE posts.id = ?
                ", array($postId))->fetch(false);
            $this->asArray = $post;
        }
    }

    public function getPost() {
        return $this->asArray;
    }

    public function create($title, $text, $user_id) {
        $this->_db->query("
                INSERT INTO posts (title, text, user_id, created_at)
                VALUES (?, ?, ?, NOW())
            ", array($title, $text, $user_id));
    }

    public function delete($postId) {
        $this->_db->delete("posts", $postId);
    }

    public function update($postId, $title, $text) {
        $this->_db->query("
                UPDATE posts 
                SET title = ?, 
                    text = ?
                WHERE id = ?
            ", array($title, $text, $postId));
    }

}
