<?php

/**
 * Description of User
 *
 * @author Vladimir
 */
class User {

    protected $_username;
    protected $_password;
    protected $_db;
    public $user;
    public $test;
    public $login_at;

    public function __construct(Db $db = null) {
        $this->_db = $db;
    }

    public function login($username, $password) {
        $this->_username = $username;
        $this->_password = $password;
        $this->login_at = time();
        $user = $this->_authenticate();
        if ($user) {
            $this->user = $user;
            $_SESSION['user'] = $user;
            return $user['id'];
        }
        return false;
    }

    protected function _authenticate() {
        $stmt = $this->_db->query("
                SELECT *
                FROM users
                WHERE username = ?
           ", array($this->_username));

        if ($stmt->count() > 0) {
            $user = $stmt->fetch(false);
            $submitted_pass = md5($this->_password);
            if ($submitted_pass == $user['password']) {
                return $user;
            }
        }
        return false;
    }

    public function checkTimeout($login_time) {
        $currentTime = time();
        $timeDifference = $currentTime - $login_time;

        if ($timeDifference > 60) {
            $this->logout();
            return true;
        } else {
            return false;
        }
    }

    public function logout() {
        unset($_SESSION['user']);
        unset($_SESSION['lastactivity']);
        $this->user = NULL;
    }
}
